from django.db import models

from .utils import ChoiceEnum


class Car(models.Model):
    # Querying requires an extra word to type though...
    # red_cars = Car.objects.filter(color=Car.Colors.RED.value)
    # Encapsulation, we meet again.
    class Colors(ChoiceEnum):
        RED = 'red'
        WHITE = 'white'
        BLUE = 'blue'

    color = models.CharField(max_length=5, choices=Colors.choices(), default=Colors.RED)
