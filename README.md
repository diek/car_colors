## Overview
## Anthony Fox's example of using enum to create a cleaner Choices field  
[Choices for Choices in Django CharFields](http://anthonyfox.io/2017/02/choices-for-choices-in-django-charfields/)
Fails on migration with:
`AttributeError: module 'cars.models' has no attribute 'Colors'`
